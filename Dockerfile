FROM archlinux:latest

RUN mkdir /tmp/img
WORKDIR /tmp/img
COPY ./ ./

RUN pacman -Syu --noconfirm git rsync

RUN pacman -S --noconfirm $(cat global/package.list) && pacman -S --noconfirm $(cat package.list)

RUN shopt -s dotglob && cp -r /tmp/img/overlay/overlay/* /

RUN shopt -s dotglob && cp -r /tmp/img/overlay/local/* /

RUN chmod +x global/fsscript.sh && chmod +x fsscript.sh && global/fsscript.sh && ./fsscript.sh

